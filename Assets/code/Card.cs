using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour 
{

	/* The deck of cards arrived with names based on the suit and the pips
         * that is, the ace of clubs was a01.bmp. The 10 of hearts was named h10.bmp. 
         * They were imported that way but would be hard to use progammactically.
         * To build an easy algorithm for manipulating the cards, the asset names were 
         * changed to three digits, zero relative, to reflect the location of 
         * the card in the deck numerically.
         * The first digit is the suit: 0=clubs, 1=diamonds, 2=hearts, 3=spades.
         * The next two digits are the pips: A=00, 2=01, ... 10=09, J=10, Q=11, K=12.
         * So the ace of clubs is 000, 0th suit, 00th card. the 10 of hearts is 209, etc.*/
         
        // int cardValue;   // 0-51  where cards are in order: A-K, & in order of suits: clubs, diamonds, hearts and then spades
        public Texture2D cardFace;

        // constructor method
        public Card(Texture2D loadedTexture)
        {
            cardFace = loadedTexture;
        }
}
